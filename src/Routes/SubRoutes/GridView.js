import React, { Component } from 'react';

import { observer, inject } from 'mobx-react';

import Typography from '@material-ui/core/Typography';

import { AgGridReact } from 'ag-grid-react';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-material.css';

import styles from './GridView.module.less';

@inject('Store')
@observer
export default class GridView extends Component {
    constructor (props) {
        super(props);

        this.state = {
            columnDefs: [
                { headerName: 'Brand', field: 'brand', pinned: 'left', sortable: true, filter: 'agTextColumnFilter' },
                { headerName: 'Model', field: 'model', sortable: true, filter: 'agTextColumnFilter' },
                { headerName: 'Price', field: 'price', editable: true, sortable: true, filter: 'agTextColumnFilter' },
                { headerName: 'Weight', field: 'weight', sortable: true, filter: 'agTextColumnFilter' },
                { headerName: 'Age', field: 'age', editable: true, sortable: true, filter: 'agTextColumnFilter', cellClassRules: {
                    [styles.ragGreen]: 'x < 3',
                    [styles.ragAmber]: 'x >= 3 && x < 7',
                    [styles.ragRed]: 'x >= 7'
                } },
                { headerName: 'Location', field: 'location', pinned: 'right', sortable: true, filter: 'agTextColumnFilter' }
            ]
        };
    }

    render () {
        return (
            <div className={styles.GridView}>
                <div className={styles.GridViewBody}>
                    <Typography paragraph>
                        {this.props.Store.dataCount} entries
                    </Typography>
                    <div className={['ag-theme-material', styles.GridViewTable].join(' ')}>
                        <AgGridReact columnDefs={this.state.columnDefs} rowData={this.props.Store.gridData} rowSelection="single" />
                    </div>
                </div>
            </div>
        );
    }
}

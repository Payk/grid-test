import React, { Component } from 'react';

import Typography from '@material-ui/core/Typography';
import { Link } from "react-router-dom";

import styles from './DashboardView.module.less';


export default class DashboardView extends Component {
    constructor (props) {
        super(props);
    }

    render () {
        return (
            <div className={styles.DashboardView}>
                <Typography variant="h3" component="h4">
                    Dashboard
                </Typography>

                <Link to="/main/grid-view">Grid sample</Link>
            </div>
        );
    }
}

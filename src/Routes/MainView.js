import React, { Component } from 'react';
import Navigation from '../Components/Navigation';

import styles from './MainView.module.less';

import { Switch, Route } from "react-router-dom";

import DashboardView from './SubRoutes/DashboardView';
import GridView from './SubRoutes/GridView';

export default class MainView extends Component {
    constructor (props) {
        super(props);
    }

    render () {
        return (
            <div className={styles.MainView}>
                <Navigation title="Grid-Demo">
                    <Switch>
                        <Route exact path="/main" component={DashboardView}/>
                        <Route path="/main/grid-view" component={GridView}/>
                    </Switch>
                </Navigation>
            </div>
        );
    }
}

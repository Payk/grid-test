import React, { Component } from 'react';

import TextField from '@material-ui/core/TextField';
import LockIcon from '@material-ui/icons/Lock';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Fab from '@material-ui/core/Fab';

import { observer, inject } from 'mobx-react';

import { NavLink } from "react-router-dom";

import styles from './LoginView.module.less';


@inject('Store')
@observer
class LoginView extends Component {
    constructor (props) {
        super(props);
    }

    render () {
        return (
            <div className={styles.LoginView}>
                <Card className={styles.card}>
                    <CardContent>
                        <form className={styles.container} noValidate autoComplete="off">
                            <TextField
                                id="standard-name"
                                label="Name"
                                className={styles.textField}
                                margin="normal"
                            />
                            <TextField
                                id="standard-password-input"
                                label="Password"
                                className={styles.textField}
                                type="password"
                                autoComplete="current-password"
                                margin="normal"
                            />
                        </form>
                    </CardContent>
                    <CardActions className={styles.actions}>
                        <NavLink to="/main/">
                            <Fab color="primary" aria-label="login">
                                <LockIcon />
                            </Fab>
                        </NavLink>
                    </CardActions>
                </Card>
            </div>
        );
    }
}

export default LoginView;


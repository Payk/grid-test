import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { Provider } from 'mobx-react';
import Store from './Store.js';

const Root = (
    <Provider Store={Store}>
        <App />
    </Provider>
);

ReactDOM.render(Root, document.getElementById("Demo-App"));

import { observable, action, computed } from 'mobx';

class AppStore {
    @observable gridData = [];

    @action addGridEntry (entry) {
        this.gridData.push(entry);
    }

    @computed get dataCount () {
        return this.gridData.length;
    }

    constructor () {
        const brandArray = ['Porsche', 'BMW', 'Mercedes', 'VW', 'Toyota'];
        const locationArray = ['Berlin', 'Hamburg', 'Köln', 'Dresden', 'Bitterfeld', 'Weisswasser', 'Zwickau', 'München'];

        for (let i = 0, n = 100000; i < n; i++) {
            const brand = brandArray[Math.floor(Math.random() * brandArray.length)];
            const model = 'Boxter';
            const price = Math.floor((Math.random() * 72000) + 1) + '€';
            const weight = Math.floor((Math.random() * 10) + 1) + 'kg';
            const age = Math.floor((Math.random() * 10) + 1);
            const location = locationArray[Math.floor(Math.random() * locationArray.length)];
            this.addGridEntry({ brand, model, price, weight, age, location });
        }
    }
}

let store = new AppStore();

export default store;

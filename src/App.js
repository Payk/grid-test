import React, { Component } from 'react';
import { HashRouter as Router, Route } from "react-router-dom";

import LoginView from './Routes/LoginView';
import MainView from './Routes/MainView';

import styles from './App.module.less';

export default class App extends Component {
    render () {
        return (
            <div className={styles.App}>
                <Router>
                    <Route path="/" exact component={LoginView} />
                    <Route path="/main/" component={MainView} />
                </Router>
            </div>
        );
    }
}
